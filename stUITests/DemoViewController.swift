//
//  DemoViewController.swift
//  stUITests
//
//  Created by Geddy on 27/5/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController {
    @IBOutlet weak var demoLabel: UILabel!
    
    @IBOutlet weak var demoImage: UIImageView!
    
    @IBOutlet weak var pageController: UIPageControl!
    
    var timer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let hasShownScreenDefault = UserDefaults.standard
        
        if hasShownScreenDefault.value(forKey: "ShouldDemoSkip") != nil {
            hasShownScreenLoad = hasShownScreenDefault.value(forKey: "ShouldDemoSkip") as! Bool
            
        }
        
        if hasShownScreenLoad {
            self.performSegue(withIdentifier: "aSegue", sender: self)
        }
        
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @IBAction func setHasShown(_ sender: UIButton) {
        hasShownScreen = true
        let hasShownScreenDefault = UserDefaults.standard
        hasShownScreenDefault.set(hasShownScreen, forKey: "ShouldDemoSkip")
        hasShownScreenDefault.synchronize()

    }
    
    func updateTimer() {
        if indicatorIncrement <= 3 {
            
            pageController.currentPage = indicatorIncrement
            demoLabel.text = labelList[indicatorIncrement]
            demoImage.image = imageList[indicatorIncrement]
            demoImage.contentMode = .scaleAspectFill
            indicatorIncrement += 1
            
        }
        if indicatorIncrement == 3 {
            indicatorIncrement = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
