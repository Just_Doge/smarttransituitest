//
//  settings.swift
//  stUITests
//
//  Created by Geddy on 27/5/17.
//  Copyright © 2017 Smilez. All rights reserved.
//

import Foundation
import UIKit
var indicatorIncrement = 0
var labelList: [String] = ["Smart Transit Plans Your Trips", "Smart Transit Shows the Transport In A Certain Area", "Smart Transit Gives you the Ability to search for your own Bus Stops"]
var imageList: [UIImage] = [UIImage(named: "demo1")!, UIImage(named: "demo2")!, UIImage(named: "demo3")!]
var hasShownScreen = false
var hasShownScreenLoad = false
